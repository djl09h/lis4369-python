# LIS4369 - Extensible Enterprise Solutions (Python)

## David Lambert

### Assignment 4 Requirements

1. Run demo.py
2. If errors, more than likely missing installations.
3. Test Python Package Installer: pip freeze
4. Research how to install any missing packages.
5. Create at least three functions that are called by the program:
	- main(): calls at least two other functions.
	- get_requirements(): displays program requirements.
	- data_analysis_2(): displays results per demo.py.
6. Display graph per instructions within demo.py.

#### README.md file should include the following items

* Assignment requirements
* Screenshots of a4_data_analysis_2 running, as per example, including graph
* Screenshots of a4_data_analysis_2 Jupyter notebook ([a4_data_analysis_2.ipynb](a4_data_analysis_2/a4_data_analysis_2.ipynb))
* Screenshots of a4 skill sets

#### Assignment Screenshots

![a4_data_analysis_2](screenshots/a4_data_analysis_2-1.png)

![a4_data_analysis_2](screenshots/a4_data_analysis_2-2.png)

*(Graph line is split due to absence of data for those records)*

#### Jupyter Notebook Screenshots

*Jupyter notebook is quite large and can be viewed as a PDF here: [a4_data_analysis_2.pdf](screenshots/a4_data_analysis_2.pdf)*

#### Skill Sets Screenshots

| ss10_dictionaries | ss11_random | ss12_temperature |
| ------------------------ | ---- | ---- |
| ![ss10_dictionaries](../skillsets/screenshots/ss10_dictionaries.png) | ![ss11_random](../skillsets/screenshots/ss11_random.png) | ![ss12_temperature](../skillsets/screenshots/ss12_temperature.png) |
