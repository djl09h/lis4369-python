# LIS4369 - Extensible Enterprise Solutions (Python)

## David Lambert

### Assignment 5 Requirements

1. Complete the tutorial: [learn_to_use_r.R](r_tutorial/learn_to_use_r.R)
2. Code and run: [lis4369_a5.R](lis4369_a5.R)
3. Include screenshots of various plots

#### README.md file should include the following items

* Assignment requirements
* Screenshots of learn_to_use_r.R and lis4369_a5.R
* Screenshots of a5 skill sets

#### Learn R Screenshots

| Four-Panel | Plot 1 | Plot 2 |
| ------------------------ | ---- | ---- |
| ![a5_learn_r_1](screenshots/a5_learn_r_1.png) | ![a5_learn_r_2](screenshots/a5_learn_r_2.png) | ![a5_learn_r_3](screenshots/a5_learn_r_3.png) |

#### Titanic Screenshots

| Four-Panel | Plot 1 | Plot 2 |
| ------------------------ | ---- | ---- |
| ![a5_titanic_1](screenshots/a5_titanic_1.png) | ![a5_titanic_2](screenshots/a5_titanic_2.png) | ![a5_titanic_3](screenshots/a5_titanic_3.png) |

#### Skill Sets Screenshots

| ss13_sphere_volume | ss14_calculator | ss15_file_write_read |
| ------------------------ | ---- | ---- |
| ![ss13_sphere_volume](../skillsets/screenshots/ss13_sphere_volume.png) | ![ss14_calculator](../skillsets/screenshots/ss14_calculator.png) | ![ss15_file_write_read](../skillsets/screenshots/ss15_file_write_read.png) |
