# LIS4369 - Extensible Enterprise Solutions (Python)

## David Lambert

### Assignment 2 Requirements

1. Must use float data type for user input
2. Overtime rate: 1.5 times hourly rate (hours over 40)
3. Holiday rate: 2.0 times hourly rate (all holiday hours)
4. Must format currency with dollar sign, and round to two decimal places
5. Create at least three functions that are called by the program:
    - main(): calls at least two other functions
    - get_requirements(): displays the program requirements
    - calculate_payroll(): calculates an individual one-week paycheck

#### README.md file should include the following items

* Assignment requirements
* Screenshots of a2_payroll application running
* Screenshots of a2_payroll Jupyter notebook ([a2_payroll.ipynb](a2_payroll/a2_payroll.ipynb))
* Screenshots of a2 skill sets

#### Assignment Screenshots

*a2_payroll (no overtime):*

![a2_payroll (no overtime)](screenshots/a2_payroll_no_ot.png)

*a2_payroll (with overtime):*

![a2_payroll (with overtime)](screenshots/a2_payroll_ot.png)

*a2_payroll Jupyter Notebook:*

![a2_payroll.ipynb](screenshots/a2_payroll_jupyter_p1.jpg)
![a2_payroll.ipynb](screenshots/a2_payroll_jupyter_p2.jpg)

*Skill Sets:*

| ss1_square_feet_to_acres | ss2_miles_per_gallon | ss3_student_percentage |
| ------------------------ | ---- | ---- |
| ![ss1_square_feet_to_acres](../skillsets/screenshots/ss1_square_ft_to_acres.png) | ![ss2_miles_per_gallon](../skillsets/screenshots/ss2_miles_per_gallon.png) | ![ss3_student_percentage](../skillsets/screenshots/ss3_student_percentage.png) |
