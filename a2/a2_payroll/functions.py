#!/usr/bin/env python3
# 2021-09-08 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

def get_requirements():
    print()
    print("Payroll Calculator")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Must use float data type for user input.\n"
        + "2. Overtime rate: 1.5 times hourly rate (hours over 40).\n"
        + "3. Holiday rate: 2.0 times hourly rate (all holiday hours).\n"
        + "4. Must format currency with dollar sign, and round to two decimal places.\n"
        + "5. Create at least three functions that are called by the program:\n"
        + "\ta. main(): calls at least two other functions.\n"
        + "\tb. get_requirements(): displays the program requirements.\n"
        + "\tc. calculate_payroll(): calculates an individual one-week paycheck\n")

def calculate_payroll():
    print("Input:")
    hours_base = float(input("Enter hours worked: "))
    hours_ot = 0.0
    hours_hol = float(input("Enter holiday hours: "))

    pay_base = float(input("Enter hourly pay rate: "))
    pay_ot = pay_base * 1.5
    pay_hol = pay_base * 2.0

    # calculate overtime
    if (hours_base > 40):
        hours_ot = hours_base - 40
        hours_base = 40

    total_base = hours_base * pay_base
    total_ot = hours_ot * pay_ot
    total_hol = hours_hol * pay_hol
    total_gross = total_base + total_ot + total_hol
    print_pay(total_base, total_ot, total_hol, total_gross)

def print_pay(total_base, total_ot, total_hol, total_gross):
    print("\nOutput:")
    print("{0:16} ${1:,.2f}".format("Base:", total_base))
    print("{0:16} ${1:,.2f}".format("Overtime:", total_ot))
    print("{0:16} ${1:,.2f}".format("Holiday:", total_hol))
    print("{0:16} ${1:,.2f}".format("Gross:", total_gross))
    print()