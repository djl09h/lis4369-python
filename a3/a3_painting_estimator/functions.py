#!/usr/bin/env python3
# 2021-09-27 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

SQFT_PER_GALLON = 350

def get_requirements():
    print()
    print("Dave's Paint Estimator")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Calculate home interior paint cost (w/o primer).\n"
        + "2. Must use float data types.\n"
        + "3. Must use SQFT_PER_GALLON constant of 350.\n"
        + "4. Must use iteration structure (i.e., loop).\n"
        + "5. Format, right-align, and round numbers to two decimal places.\n"
        + "6. Create at least five functions that are called by the program:\n"
        + "\ta. main(): calls two other functions: get_requirements() and estimate_painting_cost().\n"
        + "\tb. get_requirements(): displays the program requirements.\n"
        + "\tc. estimate_painting_cost(): calculates interior home painting and calls print functions.\n"
        + "\td. print_painting_estimate(): displays painting costs.\n"
        + "\te. print_painting_percentage(): displays painting cost percentages."
        )

def estimate_painting_cost():
    user_continue = "y"
    while user_continue == "y":
        print("\nInput:")
        sqft = float(input("Enter total interior sq ft: "))
        ppg = float(input("Enter price per gallon of paint: "))
        rate = float(input("Enter painting rate per sq ft: "))

        print("\nOutput:")
        print_painting_estimate(sqft, ppg, rate)
        print_painting_percentage(sqft, ppg, rate)

        # make sure we only get a y or n
        valid_input = False
        while valid_input != True:
            user_continue = input("Estimate another paint job? (y/n): ").lower()
            valid_input = user_continue == "y" or user_continue == "n"
    
    print("\nThank you for using Dave's Paint Estimator!")
    print("Don't forget to visit our website: https://davehome.net")
    print()

def print_painting_estimate(sqft, ppg, rate):
    gals = sqft / SQFT_PER_GALLON
    print("{0:23} {1:>9}".format("Item", "Amount"))
    print("{0:23} {1:>9,.2f}".format("Total Sq Ft:", sqft))
    print("{0:23} {1:>9,.2f}".format("Sq Ft per Gallon:", SQFT_PER_GALLON))
    print("{0:23} {1:>9,.2f}".format("Number of Gallons:", gals))
    print("{0:23} ${1:>8,.2f}".format("Paint per Gallon:", ppg))
    print("{0:23} ${1:>8,.2f}".format("Labor per Sq Ft:", rate))
    print()

def print_painting_percentage(sqft, ppg, rate):
    gals = sqft / SQFT_PER_GALLON
    total_paint = gals * ppg
    total_labor = rate * sqft
    total = total_paint + total_labor
    pct_paint = total_paint / total * 100
    pct_labor = total_labor / total * 100
    pct_total = pct_paint + pct_labor
    print("{0:9} {1:>10} {2:>12}".format("Cost", "Amount", "Percentage"))
    print("{0:9} ${1:>9,.2f} {2:>11.2f}%".format("Paint:", total_paint, pct_paint))
    print("{0:9} ${1:>9,.2f} {2:>11.2f}%".format("Labor:", total_labor, pct_labor))
    print("{0:9} ${1:>9,.2f} {2:>11.2f}%".format("Total:", total, pct_total))
    print()