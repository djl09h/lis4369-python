# LIS4369 - Extensible Enterprise Solutions (Python)

## David Lambert

### Assignment 3 Requirements

1. Calculate home interior paint cost (w/o primer).
2. Must use float data types.
3. Must use SQFT_PER_GALLON constant of 350.
4. Must use iteration structure (i.e., loop).
5. Format, right-align, and round numbers to two decimal places.
6. Create at least five functions that are called by the program:
	- main(): calls two other functions: get_requirements() and estimate_painting_cost().
	- get_requirements(): displays the program requirements.
	- estimate_painting_cost(): calculates interior home painting and calls print functions.
	- print_painting_estimate(): displays painting costs.
	- print_painting_percentage(): displays painting cost percentages.

#### README.md file should include the following items

* Assignment requirements
* Screenshots of a3_painting_estimator application running
* Screenshots of a3_painting_estimator Jupyter notebook ([a3_painting_estimator.ipynb](a3_painting_estimator/a3_painting_estimator.ipynb))
* Screenshots of a3 skill sets

#### Assignment Screenshots

![a3_painting_estimator](screenshots/a3_painting_estimator.png)

#### Jupyter Notebook Screenshots

| Page 1 | Page 2 | Page 3 |
| ------------------------ | ---- | ---- |
| ![a3_painting_estimator_jupyter](screenshots/a3_painting_estimator_jupyter_p1.png) | ![a3_painting_estimator_jupyter](screenshots/a3_painting_estimator_jupyter_p2.png) | ![a3_painting_estimator_jupyter](screenshots/a3_painting_estimator_jupyter_p3.png) |

#### Skill Sets Screenshots

| ss4_calorie_percentage | ss5_python_selection_structures | ss6_python_loops |
| ------------------------ | ---- | ---- |
| ![ss4_calorie_percentage](../skillsets/screenshots/ss4_calorie_percentage.png) | ![ss5_python_selection_structures](../skillsets/screenshots/ss5_python_selection_structures.png) | ![ss6_python_loops](../skillsets/screenshots/ss6_python_loops.png) |
