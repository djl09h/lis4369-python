# LIS4369 - Extensible Enterprise Solutions (Python)

## David Lambert

### Project 2 Requirements

1. Reverse-engineer an R Language file from screenshots.
2. Use these resources for assistance:
 	- R Manual: https://cran.r-project.org/doc/manuals/r-release/R-lang.pdf
 	- R for Data Science: https://r4ds.had.co.nz/ 
3. Include screenshots of RStudio and at least two plots

#### README.md file should include the following items

* Assignment requirements
* Link to [lis4369_p2.R](lis4369_p2.R)
* Link to [lis4369_p2_output.txt](lis4369_p2_output.txt)
* Screenshot of lis4369_p2.R running in RStudio
* Screenshots of a qplot() and a plot()

#### lis4369_p2.R Screenshots

*(Right-click an image to open larger in a new tab)*

| RStudio | Plot 1 | Plot 2 |
| ------------------------ | ---- | ---- |
| ![lis4369_p2_1](screenshots/lis4369_p2_1.png) | ![lis4369_p2_2](screenshots/lis4369_p2_2.png) | ![lis4369_p2_3](screenshots/lis4369_p2_3.png) |