#!/usr/bin/env python3
# 2021-10-12 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

from demo import *

def main():
    get_requirements()
    data_analysis_1()

if __name__ == "__main__":
    main()