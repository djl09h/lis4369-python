# LIS4369 - Extensible Enterprise Solutions (Python)

## David Lambert

### Project 1 Requirements

1. Run demo.py
2. If errors, more than likely missing installations.
3. Test Python Package Installer: pip freeze
4. Research how to do the following installations:
	- pandas (only if missing)
	- pandas-datareader (only if missing)
	- matplotlib (only if missing).
5. Create at least three functions that are called by the program:
	- main(): calls at least two other functions.
	- get_requirements(): displays program requirements.
	- data_analysis_1(): displays the following data."

#### README.md file should include the following items

* Assignment requirements
* Screenshots of p1_data_analysis application running
* Screenshots of p1_data_analysis Jupyter notebook ([p1_data_analysis.ipynb](p1_data_analysis/p1_data_analysis.ipynb))
* Screenshots of p1 skill sets (7-9)
* Screenshot of extra credit skill set: Rain Detector

#### Assignment Screenshots

| Page 1 | Page 2 | Page 3 |
| ------------------------ | ---- | ---- |
| ![p1_data_analysis](screenshots/p1_data_analysis_p1.png) | ![p1_data_analysis](screenshots/p1_data_analysis_p2.png) | ![p1_data_analysis](screenshots/p1_data_analysis_p3.png) |

#### Jupyter Notebook Screenshots

| Page 1 | Page 2 | Page 3 |
| ------------------------ | ---- | ---- |
| ![p1_data_analysis_jupyter](screenshots/p1_data_analysis_jupyter_p1.png) | ![p1_data_analysis_jupyter](screenshots/p1_data_analysis_jupyter_p2.png) | ![p1_data_analysis_jupyter](screenshots/p1_data_analysis_jupyter_p3.png) |

#### Skill Sets Screenshots

| ss7_lists | ss8_tuples | ss9_sets |
| ------------------------ | ---- | ---- |
| ![ss7_lists](../skillsets/screenshots/ss7_lists.png) | ![ss8_tuples](../skillsets/screenshots/ss8_tuples.png) | ![ss9_sets](../skillsets/screenshots/ss9_sets.png) |

#### Extra Credit Skill Set

![rain_detector](../skillsets/screenshots/rain_detector.png)