# LIS4369 - Extensible Enterprise Solutions (Python)

## David Lambert

### Assignment 1 Requirements

*Four Parts*

1. Distributed Version Control with Git and BitBucket
2. Development installations
3. Questions
4. BitBucket repo links:

    a) this assignment; and

    b) the completed tutorial (BitBucketStationLocations)

#### README.md file should include the following items

* Screenshots of a1_tip_calculator application running
* Link to A1 .jpynb file: [a1_tip_calculator.ipynb](a1_tip_calculator/tip_calculator.ipynb)
* git commands w/ short descriptions

#### Git commands w/short descriptions

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git remote - Manage set of tracked repositories

#### Assignment Screenshots

*a1_tip_calculator running (IDLE):*

![a1_tip_calculator IDLE Screenshot](screenshots/a1_tip_idle.png)

*a1_tip_calculator running (Visual Studio Code):*

![a1_tip_calculator Visual Studio Code Screenshot](screenshots/a1_tip_vsc.png)

*A1 Jupyter Notebook:*

![a1_tip_calculator.ipynb](screenshots/a1_jupyter_notebook.png)

#### Tutorial Links

[A1 Bitbucket Station Locations Tutorial](https://bitbucket.org/djl09h/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
