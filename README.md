# LIS4369 - Extensible Enterprise Solutions (Python/R Data Analytics/Visualization)

## David Lambert

### LIS4369 Requirements

*Course Work Links*

1. [A1 README.md](a1/README.md)

    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create *a1_tip_calculator* application
    - Create *a1_tip_calculator* Jupyter Notebook
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (BitBucketStationLocations)
    - Provide git command descriptions
	
2. [A2 README.md](a2/README.md)

    - Reverse-engineer a Python program: Payroll Calculator
    - Exhibit Python separation of concerns
    - Jupyter Notebook
    - Skill Sets 1-3
	
3. [A3 README.md](a3/README.md)

	- Reverse-engineer a Python program: Painting Estimator
	- Exhibit Python iteration structures (loops), function calls
	- Jupyter Notebook
	- Skill Sets 4-6

4. [A4 README.md](a4/README.md)

    - Code and run demo.py
    - Reverse-engineer demo.py to reflect running application in screenshots
    - Demonstrate the use of Python modules: re, NumPy, and Pandas
    - Jupyter Notebook
    - Skill Sets 10-12

5. [A5 README.md](a5/README.md)

    - Learn RStudio
    - Complete the tutorial: learn_to_use_r.R
    - Code and run: lis4369_a5.R
    - Include screenshots of various plots

6. [P1 README.md](p1/README.md)

    - Code and run demo.py
    - Reverse-engineer demo.py to reflect running application in screenshots
    - Install Python packages using pip
    - Jupyter Notebook
    - Skill Sets 7-9

7. [P2 README.md](p2/README.md)

    - Reverse-engineer an R Language file from screenshots
    - Use online resources for assistance
    - Include screenshots of RStudio and at least two plots