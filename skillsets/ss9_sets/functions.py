#!/usr/bin/env python3
# 2021-10-15 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

def print_requirements():
    print("Python Sets")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Sets (Python data structure): mutable, hetereogeneous unordered sequence of elements. Cannot hold duplicate values.\n"
        + "2. Sets are mutable/changeable - can insert, update, delete.\n"
        + "3. While sets are mutable/changeable, they cannot contain other mutable items like lists, sets, or dictionaries.\n"
        + "4. Since sets are unordered, they cannot use indexing or slicing to access, update, or delete elements.\n"
        + "5. Two methods to create sets:\n"
        + "\ta. Curly brackets: my_set = {1, 3.14, 2.0, 'four', 'Five'}\n"
        + "\tb. set() function: my_set = set(<iterable>)\n"
        + "6. Note: An iterable is any object that can be iterated over - lists, tuples, and even strings.\n"
        + "7. Create a program that mirrors the following IPO (input/process/output).")

def do_sets():
    my_set = {1, 3.14, 2.0, 'four', 'Five'}
    print("\nPrint my_set created using curly brackets:")
    print(my_set)

    print("\nPrint type of my_set:")
    print(type(my_set))

    my_set1 = set([1, 3.14, 2.0, 'four', 'Five'])
    print("\nPrint my_set1 created using set() method with list:")
    print(my_set1)

    print("\nPrint type of my_set1:")
    print(type(my_set1))

    my_set2 = set((1, 3.14, 2.0, 'four', 'Five'))
    print("\nPrint my_set2 created using set() method with tuple:")
    print(my_set2)

    print("\nPrint type of my_set2:")
    print(type(my_set2))

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nDiscard 'four' from my_set:")
    my_set.discard('four')
    print(my_set)

    print("\nRemove 'Five' from my_set:")
    my_set.remove('Five')
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nAdd element to my_set (4) using add() method:")
    my_set.add(4)
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))

    print("\nMinimum number in my_set:")
    print(min(my_set))

    print("\nMaximum number in my_set:")
    print(max(my_set))

    print("\nSum of numbers in my_set:")
    print(sum(my_set))

    print("\nDelete all set elements in my_set:")
    my_set.clear()
    print(my_set)

    print("\nLength of my_set:")
    print(len(my_set))