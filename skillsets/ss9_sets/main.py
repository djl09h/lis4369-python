#!/usr/bin/env python3
# 2021-10-15 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

from functions import *

def main():
    print_requirements()
    do_sets()

if __name__ == "__main__":
    main()
