#!/usr/bin/env python3
# 2021-11-15 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

import os

def print_requirements():
    print("Python File Write & Read")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Create write_read_file subdirectory with two files: main.py and functions.py.\n"
        + "2. Use President Abraham Lincoln's Gettysburg Address: Full Text.\n"
        + "3. Write the speech to a file.\n"
        + "4. Read the speech from the same file and print it to the screen.\n"
        + "5. Create and display Python Docstrings for each function in functions.py.\n"
        + "6. Print the full file path.\n"
        )

def file_write():
    """
    Usage: Creates file and writes contents of a global variable to the file.
    Parameters: none
    Returns: none
    """

    f = open("tests.txt", "w")
    f.write("Four score and seven years ago our fathers brought forth upon this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal.\n\n"
        + "Now we are engaged in a great civil war, testing whether that nation, or any nation so conceived and so dedicated, can long endure. We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final resting place for those who here gave their lives that that nation might live. It is altogether fitting and proper that we should do this.\n\n"
        + "But, in a larger sense, we can not dedicate—we can not consecrate—we can not hallow—this ground. The brave men, living and dead, who struggled here, have consecrated it, far above our poor power to add or detract. The world will little note, nor long remember what we say here, but it can never forget what they did here. It is for us the living, rather, to be dedicated here to the unfinished work which they who fought here have thus far so nobly advanced. It is rather for us to be here dedicated to the great task remaining before us—that from these honored dead we take increased devotion to that cause for which they gave the last full measure of devotion—that we here highly resolve that these dead shall not have died in vain—that this nation, under God, shall have a new birth of freedom—and that government of the people, by the people, for the people, shall not perish from the earth.\n\n"
        + "\t—Abraham Lincoln")

    f.write("\n\nFull File Path: " + os.getcwd() + f.name)
    f.close()

def file_read():
    """
    Usage: Reads contents of the file that was written to.
    Parameters: none
    Returns: none
    """
    f = open("tests.txt", "r")
    print(f.read())
    print()

def write_read_file():
    """
    Usage: Calls two functions:
    \t1. file_write() # writes to file
    \t2. file_read() # reads from file
    Parameters: none
    Returns: none
    """

    help(write_read_file)
    help(file_write)
    help(file_read)

    file_write()
    file_read()