#!/usr/bin/env python3
# 2021-09-24 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

def print_requirements():
    print("Python Selection Structures")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Use Python selection structure.\n"
        + "2. Prompt user for two numbers and a suitable operator.\n"
        + "3. Test for correct numeric operator.\n"
        + "4. Replicate display below.\n")

def calc():
    print("Python Calculator")
    num1 = float(input("Enter num1: "))
    num2 = float(input("Enter num2: "))

    print("\nSuitable operators: +, -, *, /, // (integer division), % (modulo operator), ** (power)")
    op = input("Enter operator: ")

    if op == "+":
        print(num1 + num2)
    elif op == "-":
        print(num1 - num2)
    elif op == "*":
        print(num1 * num2)
    elif op == "/":
        if num2 == 0:
            print("Cannot divide by zero.")
        else:
            print(num1 / num2)
    elif op == "//":
        if num2 == 0:
            print("Cannot divide by zero.")
        else:
            print(num1 // num2)
    elif op == "%":
        if num2 == 0:
            print("Cannot divide by zero.")
        else:
            print(num1 % num2)
    elif op == "**":
        print(num1 ** num2)
    else:
        print("Invalid operator entered. Try again next time!")
