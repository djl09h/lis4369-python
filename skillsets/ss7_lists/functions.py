#!/usr/bin/env python3
# 2021-10-13 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

def print_requirements():
    print("Python Lists")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Lists (Python data structure): mutable, ordered sequence of elements.\n"
        + "2. Lists are mutable/changeable - that is, can insert, update, delete.\n"
        + "3. Create list - using square brackets [list]: my_list = [\"cherries\", \"apples\", \"bananas\", \"oranges\"].\n"
        + "4. Create a program that mirrors the following IPO (input/process/output).\n"
        + "Note: user enters number or requested list elements, dynamically rendered below (that is, number of elements can change each run).")

def do_lists():
    print("\nInput:")
    numElements = int(input("Enter number of list elements: "))
    print()

    elements = []   # create an empty list

    for i in range(numElements):
        elements.append(input("Enter list element " + str(i+1) + ": "))

    print("\nOutput:")
    print("Print the list:")
    print(elements)

    newElement = input("\nEnter a new element to insert: ")
    newElementPos = int(input("Enter index position of new element: "))

    elements.insert(newElementPos,newElement)
    print("\nNew element inserted into the list:")
    print(elements)

    print("\nNumber of elements in the list:")
    print(len(elements))

    print("\nSort elements in list alphabetically:")
    elements.sort()
    print(elements)

    print("\nReverse list:")
    elements.reverse()
    print(elements)

    print("\nRemove last list element:")
    elements.pop()
    print(elements)

    print("\nDelete second element from list by *index*:")
    elements.pop(1)
    print(elements)

    delElement = input("\nEnter an element to delete: ")
    print("\nDelete element from list by *value* (" + delElement + "):")
    elements.remove(delElement)
    print(elements)

    print("\nDelete all elements from list:")
    elements = []
    print(elements)