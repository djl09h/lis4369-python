#!/usr/bin/env python3
# 2021-10-13 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

def print_requirements():
    print("Python Tuples")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Tuples (Python data structure): *immutable*, ordered sequence of elements.\n"
        + "2. Tuples are immutable/unchangeable - that is, cannot insert, update, delete.\n"
        + "\tNote: can reassign or delete an *entire* tuple, but not individual items or slices.\n"
        + "3. Create tuple using parantheses (tuple): my_tuple1 = (\"cherries\", \"apples\", \"bananas\", \"oranges\")\n"
        + "4. Create tuple (packing): without using parantheses.\n"
        + "5. Create tuple (unpacking): assign values from tuple to sequence of variables.\n"
        + "Create a program that mirrors the following IPO (input/process/output) format.")

def do_tuples():
    
    my_tuple1 = ('cherries', 'apples', 'bananas', 'oranges')
    my_tuple2 = 1, 2, "three", "four"

    print("\nOutput:")

    print("\nPrint my_tuple1:")
    print(my_tuple1)

    print("\nPrint my_tuple2:")
    print(my_tuple2)

    fruit1, fruit2, fruit3, fruit4 = my_tuple1
    print("\nPrint my_tuple1 unpacking:")
    print(fruit1, fruit2, fruit3, fruit4)

    print("\nPrint third element in my_tuple2:")
    print(my_tuple2[2])

    print("\nPrint \"slice\" of my_tuple2 (second and third elements):")
    print(my_tuple2[1:3])

    print("\nReassign my_tuple2 using \"packing\" method.")
    my_tuple2 = 5, 6, 7, 8

    print("Print my_tuple2:")
    print(my_tuple2)

    print("\nPrint number of elements in my_tuple1: " + str(len(my_tuple1)))

    print("\nPrint type of my_tuple1: " + str(type(my_tuple1)))

    print("\nDeleting my_tuple1. ")
    del my_tuple1

    print()