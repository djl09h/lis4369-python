#!/usr/bin/env python3
# 2021-10-31 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

import random

def print_requirements():
    print("Pseudo-Random Number Generator")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Get from user beginning and ending int values and stor in two variables.\n"
        + "2. Display ten psuedo-random numbers between and including above values.\n"
        + "3. Must use integer data types."
    )

def do_rando():
    start = 0
    end = 0

    print("\nInput: ")
    start = int(input("Enter beginning value: "))
    end = int(input("Enter ending value: "))

    print("\nOutput:")
    print("Example 1: Using range() and randint() functions:")
    for item in range(10):
        print(random.randint(start,end), sep=",", end=" ")
    print()

    print("\nExample 2: Using a list, with range() and shuffle() functions:")
    my_list = list(range(start, end + 1))
    random.shuffle(my_list)
    for item in my_list:
        print(item, sep=",", end=" ")
    print()