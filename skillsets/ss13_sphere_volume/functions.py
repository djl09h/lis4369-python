#!/usr/bin/env python3
# 2021-11-01 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

import math as m

def print_requirements():
    print("Sphere Volume")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Calculates sphere volume in U.S. gallons from user-entered diameter value in inches, and rounds to two decimal places.\n"
        + "2. Must use Python's built-in pi and pow() capabilities.\n"
        + "3. Program checks for non-integer and non-numeric values.\n"
        + "4. Continues to prompt for user entry until no longer requested. Prompt accepts upper or lower case."
        )

def calc_sphere():
    while True:
        userInput = input("\nPlease enter diameter in inches, or 'q' to exit: ").lower()

        if userInput == 'q':
            break

        if userInput.isnumeric() == False:
            print("\nNot a valid integer! Try again.")
            continue
        else:
            result = (m.pow(int(userInput)/2,3) * (m.pi) * (4 / 3)) / 231
            print("\nSphere volume: " + str(round(result,2)))