#!/usr/bin/env python3
# 2021-10-31 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

def print_requirements():
    print("Python Dictionaries")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Dictionaries (Python data structure): unordered key:value pairs.\n"
        + "2. Dictionary: an associative array (also known as hashes).\n"
        + "3. Any key in a dictionary is associated (or mapped) to a value.\n"
        + "4. Keys: must be of immutable type and must be unique.\n"
        + "5. Values: can be any data type and can repeat.\n"
        + "6. Create a program that mirrors the following IPO:\n"
        + "\ta. Create empty dictionary using curly braces {}: my_dictionary = {}\n"
        + "\tb. Use the following keys: fname, lname, degree, major, gpa\n"
        + "Note: Dictionaries have key-value pairs instead of single values - this differentiates dictionaries from sets.")

def do_dicts():
    my_dictionary = {}
    #my_dictionary = { 'fname':'Mark', 'lname':'Jowett', 'degree':'BSIT', 'major':'IT', 'gpa':4.0 }
    
    print("\nInput:")

    my_dictionary['fname'] = str(input("First Name: "))
    my_dictionary['lname'] = str(input("Last Name: "))
    my_dictionary['degree'] = str(input("Degree: "))
    my_dictionary['major'] = str(input("Major: "))
    my_dictionary['gpa'] = float(input("GPA: "))

    print("\nOutput:")

    print("Print my_dictionary:")
    print(my_dictionary)

    print("\nReturn view of dictionary's key:value pair (built-in function):")
    print(my_dictionary.items())

    print("\nReturn view object of all keys (built-in function):")
    print(my_dictionary.keys())

    print("\nReturn view object of all values (built-in function):")
    print(my_dictionary.values())

    print("\nPrint only first and last names, using keys:")
    print(my_dictionary['fname'] + " " + my_dictionary['lname'])

    print("\nPrint only first and last names, using get() function:")
    print(my_dictionary.get('fname') + " " + my_dictionary.get('lname'))

    print("\nCount number of items (key:value pairs):")
    print(len(my_dictionary))

    print("\nRemove last dictionary item:")
    my_dictionary.popitem()
    print(my_dictionary)

    print("\nDelete major from dictionary (using key):")
    my_dictionary.pop('major')
    print(my_dictionary)

    print("\nReturn object type:")
    print(type(my_dictionary))

    print("\nDelete all items from dictionary:")
    my_dictionary.clear()
    print(my_dictionary)