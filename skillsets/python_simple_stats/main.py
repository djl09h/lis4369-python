#!/usr/bin/env python3
# 2021-11-10 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

# If it's a weekday, it's raining. If weekend, it's False (not raining)
# try to use exclusive or operator

import functions as f

def main():
    f.print_requirements()
    f.get_stats()

if __name__ == "__main__":
    main()