#!/usr/bin/env python3
# 2021-11-10 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

import statistics as s

my_list = [37,32,46,28,37,41,31]

def print_requirements():
    print()
    print("Rain Detector")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Create stats subdirectory with two files: main.py and functions.py.\n"
        + "2. Create integer list (see below).\n"
        + "3. Create user-defined get_range() function.\n"
        + "4. Use Python intrinsic (built-in) functions from \"statistics\" module.\n"
        )

def get_range(my_list):
    return max(my_list) - min(my_list)

def get_stats():
    print("my_list: " + str(my_list))
    print("Ascending Order: ")
    print("Descending Order: ")
    print("Count: " + str(len(my_list)))
    print("Sum: " + str(sum(my_list)))
    print("Min: " + str(min(my_list)))
    print("Max: " + str(max(my_list)))