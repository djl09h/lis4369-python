#!/usr/bin/env python3
# 2021-09-08 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

def print_requirements():
    print()
    print("IT/ICT Student Percentage")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Find number of IT/ICT students in class.\n"
        + "2. Calculate IT/ICT student percentage."
        + "3. Must use float data type, to facilitate right-alignment.\n"
        + "4. Format, right-align, and round numbers to two decimal places.\n")

def calc_percentages():
    print("Input:")
    students_it = float(input("Enter number of IT students: "))
    students_ict = float(input("Enter number of ICT students: "))

    students_total = students_it + students_ict
    students_it_pct = (students_it / students_total) * 100
    students_ict_pct = (students_ict / students_total) * 100

    print("\nOutput:")
    print("{0:16} {1:,.2f}".format("Total Students:", students_total))
    print("{0:16} {1:,.2f}{2}".format("IT Students:", students_it_pct, "%"))
    print("{0:16} {1:,.2f}{2}".format("ICT Students:", students_ict_pct, "%"))
    print()