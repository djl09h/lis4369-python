#!/usr/bin/env python3
# 2021-09-20 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

def print_requirements():
    print("Calorie Percentage")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Find calories per gram of fat, carbs, and protein.\n"
        + "2. Calculate percentages."
        + "3. Must use float data types.\n"
        + "4. Format, right-align, and round numbers to two decimal places.\n")

def calc_percentages():
    print("Input:")
    fat = float(input("Enter total fat grams: "))
    carbs = float(input("Enter total carb grams: "))
    protein = float(input("Enter total protein grams: "))

    grams_total = fat + carbs + protein     # calc total grams
    
    # calculate calories
    cal_fat = fat * 9
    cal_carbs = carbs * 4
    cal_protein = protein * 4

    cal_total = cal_fat + cal_carbs + cal_protein   # calc total cals
    
    # calculate percentages
    pct_fat = cal_fat / cal_total * 100
    pct_carbs = cal_carbs / cal_total * 100
    pct_protein = cal_protein / cal_total * 100


    print("\nOutput:")
    print("{0:10} {1:>12} {2:>13}".format("Type", "Calories", "Percentage"))
    print("{0:10} {1:>12,.2f} {2:>12,.2f}{3}".format("Fat:", cal_fat, pct_fat, "%"))
    print("{0:10} {1:>12,.2f} {2:>12,.2f}{3}".format("Carbs:", cal_carbs, pct_carbs, "%"))
    print("{0:10} {1:>12,.2f} {2:>12,.2f}{3}".format("Protein:", cal_protein, pct_protein, "%"))
    print()