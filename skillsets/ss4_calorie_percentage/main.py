#!/usr/bin/env python3
# 2021-09-20 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

from functions import *

def main():
    print_requirements()
    calc_percentages()

if __name__ == "__main__":
    main()