#!/usr/bin/env python3
# 2021-11-03 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

import math as m

def print_requirements():
    print("Python Calculator w/ Error Handling")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Calculates two numbers and rounds to two decimal places.\n"
        + "2. Prompts user for two numbers and a suitable operator.\n"
        + "3. Uses Python error handling to validate data.\n"
        + "4. Tests for correct arithmetic operator.\n"
        + "5. Divsion by zero is not permitted.\n"
        + "6. Program loops until correct input is entered."
        )

def getNum(prompt):
    while True:
        try:
            return float(input("\n" + prompt + " "))
        except ValueError:
            print("Not a number! Try again!")

def getOp():
    validOperators = ['+','-','*','/','//','%','**']
    while True:
        op = input("\nSuitable Operators: +, -, *, /, // (integer division), % (modulo operator), ** (power): ")
        try:
            validOperators.index(op) # check if user-entered operator is in the list
            return op
        except ValueError:
            print("Invalid operator! Try again!")

def calc():
    num1 = getNum("Enter num1:")
    num2 = getNum("Enter num2:")
    op = getOp()
    sum = 0.0

    if op == '+':
        sum = num1 + num2
    elif op == '-':
        sum = num1 - num2
    elif op == '*':
        sum = num1 * num2
    elif op == '**':
        sum = num1 ** num2
    elif op == '%':
        while True:
            try:
                sum = num1 % num2
                break   # if we get here, we threw no exceptions
            except ZeroDivisionError:
                num2 = getNum("Hey! You can't divide by zero! Re-enter num2:") # re-prompt for num2
    elif op == '/':
        while True:
            try:
                sum = num1 / num2
                break   # if we get here, we threw no exceptions
            except ZeroDivisionError:
                num2 = getNum("Hey! You can't divide by zero! Re-enter num2:") # re-prompt for num2
    elif op == '//':
        while True:
            try:
                sum = num1 // num2
                break   # if we get here, we threw no exceptions
            except ZeroDivisionError:
                num2 = getNum("Hey! You can't divide by zero! Re-enter num2:") # re-prompt for num2

    print("\nAnswer is " + str(round(sum,2)))
    print()