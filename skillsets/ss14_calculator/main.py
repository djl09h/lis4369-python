#!/usr/bin/env python3
# 2021-11-03 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

import functions as f

def main():
    f.print_requirements()
    f.calc()

if __name__ == "__main__":
    main()