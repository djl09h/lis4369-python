#!/usr/bin/env python3
# 2021-09-08 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

def print_requirements():
    print("Miles Per Gallon")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Calculate MPG.\n"
        + "2. Must use float data type for user input and calculation.\n"
        + "3. Format and round conversion to two decimal places.\n")

def calc_mpg():
    print("Input:")
    miles = float(input("Enter miles driven: "))
    gals = float(input("Enter gallons of fuel used: "))

    mpg = miles / gals

    print("\nOutput:")
    print("{0:,.2f} {1} {2:,.2f} {3} {4:,.2f}".format(miles, "miles driven and", gals, "gallons used =", mpg))