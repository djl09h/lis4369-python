#!/usr/bin/env python3
# 2021-09-22 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

DAYS = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]

def print_requirements():
    print()
    print("Rain Detector")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Capture user-entered input.\n"
        + "2. Based on day of week, determine if rain will occur.\n"
        + "Note: Program does case-insensitive comparison.\n")

def calc_rain():
    print("Input:")
    day = input("Enter full name day of week: ").lower()
    dayIndex = DAYS.index(day) if day in DAYS else -1

    print("\nOutput:")
    if dayIndex < 0:
        print("Invalid day entered!\n")
        exit()
    elif dayIndex >= 0 and dayIndex < 5:
        print("{0} {1}. {2}".format("It's", day.title(), "You know it's gonna rain!\n"))
    else:
        print("{0} {1}? {2}".format("Rain on", day.title(), "No chance!\n"))