#!/usr/bin/env python3
# 2021-09-22 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

# If it's a weekday, it's raining. If weekend, it's False (not raining)
# try to use exclusive or operator

from functions import *

def main():
    print_requirements()
    calc_rain()

if __name__ == "__main__":
    main()