#!/usr/bin/env python3
# 2021-10-31 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

def print_requirements():
    print("Temperature Conversion")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Convert user-entered temperatures into Fahrenheit or Celsius.\n"
        + "2. Continue to prompt use for data entry until no longer requested.\n"
        + "3. Upper or lower case letters permitted. Incorrect entries are not permitted.\n"
        + "4. Not required to validate numeric data (but it does)."
        )

def is_number(string):
    # quick function to replace isnumeric(), to account for floats
    try:
        float(string)
        return True
    except ValueError:
        return False

def temp_convert():
    while True:
        # convert input to lowercase and remove whitespace
        userInput = input("\nEnter temperature you wish to convert (including unit C or F), or 'q' to exit: ").lower().replace(" ", "")

        # check to see if user wants to exit
        if userInput[0] == 'q':
            break

        # parse the input - assume the last character is the unit and all preceding is the number
        fromVal, fromUnit = userInput[:-1], userInput[-1]

        # validate input from use
        # ensure fromVal is a number and fromUnit is either 'f' or 'c'
        if (is_number(fromVal) == False) or (is_number(fromUnit) == True) or ((fromUnit != 'f') and (fromUnit != 'c')):
            print("\nInvalid input. Try again!")
            continue

        # now we can safely convert to a float
        fromVal = float(fromVal)

        # ...and do the actual conversion
        if fromUnit == 'f':
            print(str(fromVal) + "°F is " + str(((fromVal-32)*5)/9) + "°C")
        if fromUnit == 'c':
            print(str(fromVal) + "°C is " + str((fromVal*9/5)+32) + "°F")
    
    # if we got here, it's time to leave
    print("\nIt was fun. Goodbye!")