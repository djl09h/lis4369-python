#!/usr/bin/env python3
# 2021-09-08 Dave Lambert djl09h@my.fsu.edu
# LIS4369 Jowett - Fall 2021 FSU

SQFT_PER_ACRE = 43560

def print_requirements():
    print("Square Feet to Acres")
    print("Developer: Dave Lambert\n")
    print("Program Requirements:\n"
        + "1. Research: number of square feet to acre of land.\n"
        + "2. Must use float data type for user input and calculation.\n"
        + "3. Format and round conversion to two decimal places.\n")

def sqft_to_acre():
    print("Input:")
    sqft = float(input("Enter square feet: "))
    acres = sqft / SQFT_PER_ACRE

    print("\nOutput:")
    print("{0:,.2f} {1} {2:,.2f} {3}".format(sqft, "square feet = ", acres, "acres"))